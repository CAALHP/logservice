﻿using System;
using caalhp.IcePluginAdapters;

namespace CAALHP.SOA.ICE.LogService
{
    class Program
    {
        static void Main(string[] args)
        {
            //we assume the host is running on localhost:
            const string endpoint = "localhost";
            try
            {
                //creating the ServiceAdapter - which also handles connection to the endpoint. 
                //The second parameter is a reference to the implementation of an IServiceCAALHPContract.
                //Debugger.Launch();
                var logImp = new LogImplementation();
                var adapter = new ServiceAdapter(endpoint, logImp);
                Console.WriteLine("LogService running");
                Console.WriteLine("press <enter> to exit...");
                Console.ReadLine();
                logImp.ShutDown();
            }
            catch (Exception ex)
            {
                //Connection to host probably failed
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }
    }
}
